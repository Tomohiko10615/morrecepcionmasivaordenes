package com.enel.recepcionmasivaordenes.app.service;

import com.enel.recepcionmasivaordenes.app.dto.OrdTransferDTO;

public interface EorOrdTransferService {

	OrdTransferDTO obtenerOrdTransfer(String numeroOrden);

}
