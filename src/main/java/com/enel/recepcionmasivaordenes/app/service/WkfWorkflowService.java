package com.enel.recepcionmasivaordenes.app.service;

public interface WkfWorkflowService {

	void actualizarWorkflow(String idState, Long idWorkflow);

}
