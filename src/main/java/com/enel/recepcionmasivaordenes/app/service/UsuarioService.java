package com.enel.recepcionmasivaordenes.app.service;

public interface UsuarioService {

	Long obtenerIdUsuario(String username);

}
