package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.repository.s4j.NucEmpresaRepository;
import com.enel.recepcionmasivaordenes.app.service.NucEmpresaService;

@Service
public class NucEmpresaServiceImpl implements NucEmpresaService {

	@Autowired
	private NucEmpresaRepository nucEmpresaRepository;
	
	@Override
	public Integer obtenerIdEmpresa(String aCodPartition) {
		return nucEmpresaRepository.obtenerIdEmpresa(aCodPartition);
	}

}
