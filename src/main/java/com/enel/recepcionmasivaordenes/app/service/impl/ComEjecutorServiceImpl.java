package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.repository.s4j.ComEjecutorRepository;
import com.enel.recepcionmasivaordenes.app.service.ComEjecutorService;

@Service
public class ComEjecutorServiceImpl implements ComEjecutorService {

	@Autowired
	private ComEjecutorRepository comEjecutorRepository;
	
	@Override
	public Long obtenerIdEjecutor(String codEjecutor, Integer idEmpresa) {
		return comEjecutorRepository.obtenerIdEjecutor(codEjecutor, idEmpresa);
	}

}
