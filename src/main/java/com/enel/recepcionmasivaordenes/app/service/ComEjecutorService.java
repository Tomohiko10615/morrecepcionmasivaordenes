package com.enel.recepcionmasivaordenes.app.service;

public interface ComEjecutorService {

	Long obtenerIdEjecutor(String codEjecutor, Integer idEmpresa);

}
