package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.dto.ComponenteAuxDTO;
import com.enel.recepcionmasivaordenes.app.dto.ComponenteDTO;
import com.enel.recepcionmasivaordenes.app.repository.s4j.MedComponenteRepository;
import com.enel.recepcionmasivaordenes.app.service.MedComponenteService;

@Service
public class MedComponenteServiceImpl implements MedComponenteService {

	@Autowired
	private MedComponenteRepository medComponenteRepository;
	
	@Override
	public ComponenteDTO obtenerComponente(Long dIdServicioH, String nroComponente, String codModelo, String codMarca) {
		return medComponenteRepository.obtenerComponente(dIdServicioH, nroComponente, codModelo, codMarca);
	}

	@Override
	public ComponenteAuxDTO obtenerComponenteAux(Long dIdServicioH) {
		return medComponenteRepository.obtenerComponenteAux(dIdServicioH);
	}

}
