package com.enel.recepcionmasivaordenes.app.service;

import com.enel.recepcionmasivaordenes.app.dto.SituacionTerrenoDTO;

public interface ComSitTerrenoService {

	SituacionTerrenoDTO obtenerSituacionTerreno(String codSitTerr, Integer idEmpresa);

}
