package com.enel.recepcionmasivaordenes.app.service;

import com.enel.recepcionmasivaordenes.app.dto.ServicioDTO;

public interface NucServicioService {

	ServicioDTO obtenerServicio(Integer idEmpresa, Long dNroServicio);

}
