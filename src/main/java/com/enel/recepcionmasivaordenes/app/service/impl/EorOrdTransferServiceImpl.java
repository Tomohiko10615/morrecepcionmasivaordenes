package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.dto.OrdTransferDTO;
import com.enel.recepcionmasivaordenes.app.repository.scom.EorOrdTransferRepository;
import com.enel.recepcionmasivaordenes.app.service.EorOrdTransferService;

@Service
public class EorOrdTransferServiceImpl implements EorOrdTransferService {

	@Autowired
	private EorOrdTransferRepository eorOrdTransferRepository;
	
	@Override
	public OrdTransferDTO obtenerOrdTransfer(String numeroOrden) {
		return eorOrdTransferRepository.obtenerOrdTransfer(numeroOrden);
	}

}
