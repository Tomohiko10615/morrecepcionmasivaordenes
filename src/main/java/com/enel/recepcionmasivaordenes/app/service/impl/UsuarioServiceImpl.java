package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.repository.s4j.UsuarioRepository;
import com.enel.recepcionmasivaordenes.app.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public Long obtenerIdUsuario(String username) {
		return usuarioRepository.obtenerUsuarioId(username);
	}

}
