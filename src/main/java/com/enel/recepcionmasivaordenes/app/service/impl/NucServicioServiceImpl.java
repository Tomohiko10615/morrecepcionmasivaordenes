package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.dto.ServicioDTO;
import com.enel.recepcionmasivaordenes.app.repository.s4j.NucServicioRepository;
import com.enel.recepcionmasivaordenes.app.service.NucServicioService;

@Service
public class NucServicioServiceImpl implements NucServicioService {

	@Autowired
	private NucServicioRepository nucServicioRepository;
	
	@Override
	public ServicioDTO obtenerServicio(Integer idEmpresa, Long dNroServicio) {
		return nucServicioRepository.obtenerServicio(idEmpresa, dNroServicio);
	}

}
