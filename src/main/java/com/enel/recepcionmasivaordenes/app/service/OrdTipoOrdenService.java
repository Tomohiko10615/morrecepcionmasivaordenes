package com.enel.recepcionmasivaordenes.app.service;

import com.enel.recepcionmasivaordenes.app.dto.TipoOrdenDTO;

public interface OrdTipoOrdenService {

	TipoOrdenDTO obtenerTipoOrden(String aCodTipoOrden, Integer idEmpresa);

}
