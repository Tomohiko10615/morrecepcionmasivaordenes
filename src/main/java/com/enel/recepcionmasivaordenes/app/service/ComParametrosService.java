package com.enel.recepcionmasivaordenes.app.service;

import com.enel.recepcionmasivaordenes.app.dto.CodigoErrorDTO;

public interface ComParametrosService {

	Long obtenerEstado(String codigo);

	CodigoErrorDTO obtenerEstadoError(String codigo);

}
