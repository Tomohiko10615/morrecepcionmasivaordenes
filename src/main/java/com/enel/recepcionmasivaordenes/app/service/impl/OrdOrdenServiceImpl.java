package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enel.recepcionmasivaordenes.app.dto.OrdenDTO;
import com.enel.recepcionmasivaordenes.app.repository.s4j.OrdOrdenRepository;
import com.enel.recepcionmasivaordenes.app.service.OrdOrdenService;

@Service
public class OrdOrdenServiceImpl implements OrdOrdenService {

	@Autowired
	private OrdOrdenRepository ordOrdenRepository;
	
	@Override
	public OrdenDTO obtenerOrden(Long dIdTipoOrdenH, String numeroOrden) {
		return ordOrdenRepository.obtenerOrden(dIdTipoOrdenH, numeroOrden);
	}

	@Override
	public OrdenDTO obtenerOrden(String aCodTipoOrden, String numeroOrden) {
		return ordOrdenRepository.obtenerOrden(aCodTipoOrden, numeroOrden);
	}

	@Override
	@Transactional
	public void actualizarOrdOrden(String fechaEjec, Long dIdOrden) {
		ordOrdenRepository.actualizarOrdOrden(fechaEjec, dIdOrden);
		
	}

}
