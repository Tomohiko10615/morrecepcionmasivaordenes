package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.dto.TipoOrdenDTO;
import com.enel.recepcionmasivaordenes.app.repository.s4j.OrdTipoOrdenRepository;
import com.enel.recepcionmasivaordenes.app.service.OrdTipoOrdenService;

@Service
public class OrdTipoOrdenServiceImpl implements OrdTipoOrdenService {

	@Autowired
	private OrdTipoOrdenRepository ordTipoOrdenRepository;
	
	@Override
	public TipoOrdenDTO obtenerTipoOrden(String aCodTipoOrden, Integer idEmpresa) {
		return ordTipoOrdenRepository.obtenerTipoOrden(aCodTipoOrden, idEmpresa);
	}

}
