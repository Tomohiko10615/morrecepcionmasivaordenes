package com.enel.recepcionmasivaordenes.app.service;

public interface CorEstSrvElectricoService {

	String obtenerEstado(String aEstadoSE, String sauxCodInterno, String sCodInternoAccion);

}
