package com.enel.recepcionmasivaordenes.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionmasivaordenes.app.repository.s4j.WkfWorkflowRepository;
import com.enel.recepcionmasivaordenes.app.service.WkfWorkflowService;

@Service
public class WkfWorkflowServiceImpl implements WkfWorkflowService {

	@Autowired
	private WkfWorkflowRepository wkfWorkflowRepository;
	
	@Override
	public void actualizarWorkflow(String idState, Long idWorkflow) {
		wkfWorkflowRepository.actualizarWorkflow(idState, idWorkflow);
	}

}
