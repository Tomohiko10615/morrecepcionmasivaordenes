package com.enel.recepcionmasivaordenes.app.datasource;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.enel.recepcionmasivaordenes.app.repository.s4j", 
entityManagerFactoryRef = "synergiaEntityManagerFactory", transactionManagerRef = "synergiaTransactionManager")
public class S4JDataSourceConfiguration {

	@Bean(name = "synergiaDataSourceProperties")
	@ConfigurationProperties("oracle.datasource")
	public DataSourceProperties synergiaDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean(name = "synergiaDataSource")
	@ConfigurationProperties("oracle.datasource.configuration")
	public DataSource synergiaDataSource(
			@Qualifier("synergiaDataSourceProperties") DataSourceProperties synergiaDataSourceProperties) {
		return synergiaDataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}

	@Bean(name = "synergiaEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean synergiaEntityManagerFactory(
			EntityManagerFactoryBuilder synergiaEntityManagerFactoryBuilder,
			@Qualifier("synergiaDataSource") DataSource synergiaDataSource) {

		Map<String, String> synergiaJpaProperties = new HashMap<>();
		synergiaJpaProperties.put("hibernate.dialect", "org.hibernate.dialect.OracleDialect");
		//synergiaJpaProperties.put("spring.jpa.show-sql", "true");
		

		return synergiaEntityManagerFactoryBuilder.dataSource(synergiaDataSource)
				.packages("com.enel.recepcionmasivaordenes.app.entity.s4j").persistenceUnit("synergiaDataSource")
				.properties(synergiaJpaProperties).build();
	}

	@Bean(name = "synergiaTransactionManager")
	public PlatformTransactionManager synergiaTransactionManager(
			@Qualifier("synergiaEntityManagerFactory") EntityManagerFactory synergiaEntityManagerFactory) {

		return new JpaTransactionManager(synergiaEntityManagerFactory);
	}

}
