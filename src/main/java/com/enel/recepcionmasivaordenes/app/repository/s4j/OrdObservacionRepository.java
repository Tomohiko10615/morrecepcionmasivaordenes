package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.entity.s4j.OrdObservacion;

@Repository
public interface OrdObservacionRepository extends JpaRepository<OrdObservacion, Long>{

	@Query(value="SELECT SQOBSERVACIONORDEN.NEXTVAL FROM dual", nativeQuery=true)
	Long obtenerIdObservacion();

	public static final String SQL_ORD_OBSERVACION_UPDATE = "INSERT into ORD_OBSERVACION "
			+ "("
			+ "ID_OBSERVACION, ID_ORDEN, TEXTO, FECHA_OBSERVACION, ID_USUARIO, ID_EMPRESA, STATE_NAME, DISCRIMINATOR "
			+ ") "
			+ "VALUES "
			+ "("
			+ "?, ?, 'Anulación masiva desde interface Recepcion eOrder', sysdate, ?, ? ,'Anulada','ObeservacionOrden' "
			+ ")";
}
