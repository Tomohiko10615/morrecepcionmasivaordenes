package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.entity.s4j.CorOrden;

@Repository
public interface CorOrdenRepository extends JpaRepository<CorOrden, Long> {

	public static final String SQL_COR_ORDEN_UPDATE = "update cor_orden "
			+ "set id_ejecutor = ? "
			+ ",obs_recepcion  = ? "
			+ ",fecha_ejecucion = to_date(?,'ddMMyyyy hh24:mi') "
			+ "where id_orden = ?";
	
	public static final String SQL_COR_ORDEN_OBS_UPDATE = "update cor_orden "
			+ "set obs_recepcion  = ? "
			+ "where id_orden = ?";
}
