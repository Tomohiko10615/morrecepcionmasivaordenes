package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enel.recepcionmasivaordenes.app.dto.OrdenDTO;
import com.enel.recepcionmasivaordenes.app.entity.s4j.OrdOrden;

@Repository
public interface OrdOrdenRepository extends JpaRepository<OrdOrden, Long>{

	@Query(value="SELECT a.id_orden AS dIdOrdenH, "
			+ "a.id_workflow AS dIdWorkflowOrd, "
			+ "b.id_state AS aEstadoOrden, "
			+ "nvl(c.nivel,1) AS dNivel "
			+ "FROM ord_orden a, wkf_workflow b, cor_orden c "
			+ "WHERE a.id_tipo_orden = ?1 "
			+ "AND a.nro_orden = ?2 "
			+ "and a.id_workflow = b.id_workflow "
			+ "and a.id_orden = c.id_orden"
			, nativeQuery=true)
	OrdenDTO obtenerOrden(Long dIdTipoOrdenH, String numeroOrden);

	@Query(value="SELECT a.id_orden AS dIdOrdenH, a.id_workflow AS dIdWorkflowOrd "
			+ "FROM ord_orden a, wkf_workflow b, cor_orden c, ord_tipo_orden oto "
			+ "WHERE oto.cod_tipo_orden = ?1 "
			+ "AND a.id_tipo_orden = oto.id_tipo_orden "
			+ "AND a.nro_orden = ?2 "
			+ "AND a.id_workflow = b.id_workflow "
			+ "AND a.id_orden = c.id_orden", nativeQuery=true)
	OrdenDTO obtenerOrden(String aCodTipoOrden, String numeroOrden);

	@Transactional
	@Modifying
	@Query(value="update ord_orden "
			+ "set fecha_finalizacion = to_date(?1,'ddMMyyyy hh24:mi') "
			+ ",fecha_ingreso_estado_actual = sysdate "
			+ "where id_orden = ?2", nativeQuery=true)
	void actualizarOrdOrden(String fechaEjec, Long dIdOrden);

	public static final String SQL_ORD_ORDEN_UPDATE = "update ord_orden "
			+ "set fecha_finalizacion = to_date(?,'ddMMyyyy hh24:mi') "
			+ ",fecha_ingreso_estado_actual = sysdate "
			+ "where id_orden = ?";
	
	public static final String SQL_ORD_ORDEN_FEC_UPDATE = "UPDATE ord_orden SET "
			+ "fecha_ingreso_estado_actual = sysdate "
			+ "WHERE id_orden = ?";
}
