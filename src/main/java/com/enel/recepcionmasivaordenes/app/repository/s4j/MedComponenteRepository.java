package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.dto.ComponenteAuxDTO;
import com.enel.recepcionmasivaordenes.app.dto.ComponenteDTO;
import com.enel.recepcionmasivaordenes.app.entity.s4j.MedComponente;

@Repository
public interface MedComponenteRepository extends JpaRepository<MedComponente, Long> {

	@Query(value="select a.id_componente as dIdComponente, "
			+ "b.ID_MEDIDA as dIdMedida, "
			+ "b.CANT_ENTEROS as dEnteros, "
			+ "b.CANT_DECIMALES as dDecimales, "
			+ "b.VAL_FACTOR as dFactor, "
			+ "b.ID_TIP_CALCULO as dIdTipoCalculo "
			+ "from med_componente a, med_medida_medidor b, med_medida c, "
			+ "med_modelo mmod, med_marca mma "
			+ "where a.id_componente = b.id_componente "
			+ "and b.ID_MEDIDA = c.ID_MEDIDA "
			+ "and c.COD_INTERNO = 'ActivaFueraPunta' "
			+ "and a.ID_UBICACION = ?1 "
			+ "and type_ubicacion = 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico' "
			+ "and nro_componente = ?2 "
			+ "and mmod.id_modelo = a.id_modelo "
			+ "and mma.ID_MARCA = mmod.ID_MARCA "
			+ "and mmod.COD_MODELO = ?3 "
			+ "and mma.COD_MARCA = ?4", nativeQuery=true)
	ComponenteDTO obtenerComponente(Long dIdServicioH, String nroComponente, String codModelo, String codMarca);

	@Query(value="SELECT "
			+ "NVL(pc.id_tip_acometida,-1) AS idTipoAcometida, "
			+ "NVL(decode(mm1.id_fase,NULL,pc.id_fase,mm1.id_fase),-1) AS idFase, "
			+ "NVL(ft.id_tarifa_base,-1) AS idTarifa, "
			+ "decode(mci.ID_COND_INSTALACION,NULL,'N','S') AS tapaRanura, "
			+ "NVL(sp.VALOR,-1) AS potenciaContratada, "
			+ "szc.id_sector_tipico sectorTipico "
			+ "FROM srv_electrico se, "
			+ "med_componente mc, "
			+ "med_modelo mm1, "
			+ "med_medida_medidor mmm, "
			+ "med_medida mm, "
			+ "fac_tarifa ft, "
			+ "med_comp_cond_instalac mcci, "
			+ "med_cond_instalacion mci, "
			+ "srv_potencia sp, "
			+ "srv_tip_potencia stp, "
			+ "fac_franja_horaria ffh, "
			+ "prd_conexion pc, "
			+ "srv_zona_concesion szc "
			+ "WHERE se.id_servicio = ?1 "
			+ "And se.id_servicio = mc.ID_UBICACION (+) "
			+ "and mc.TYPE_UBICACION (+) = 'com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico' "
			+ "and mc.id_modelo = mm1.id_modelo (+) "
			+ "and mc.ID_COMPONENTE = mmm.ID_COMPONENTE (+) "
			+ "and mmm.ID_MEDIDA = mm.ID_MEDIDA (+) "
			+ "and mm.COD_INTERNO (+) = 'ActivaFueraPunta' "
			+ "and se.ID_TARIFA  = ft.id_tarifa "
			+ "and mc.ID_COMPONENTE = mcci.ID_COMPONENTE (+) "
			+ "and mcci.ID_COND_INSTALACION = mci.ID_COND_INSTALACION (+) "
			+ "and mci.COD_INTERNO (+) = 'MedidorTapaRanura' "
			+ "and sp.ID_SERVICIO = se.id_servicio "
			+ "and sp.ID_TIP_POTENCIA = stp.ID_TIP_POTENCIA "
			+ "and stp.COD_INTERNO = 'Contratada' "
			+ "and sp.ID_FRANJA = ffh.ID_FRANJA "
			+ "and ffh.COD_INTERNO = 'FueraPunta' "
			+ "and se.id_conexion = pc.id_vta_conexion "
			+ "and se.id_zona_concesion = szc.id_zona_concesion "
			+ "and rownum < 2", nativeQuery=true)
	ComponenteAuxDTO obtenerComponenteAux(Long dIdServicioH);

	public static final String SQL_ID_MAGNITUD = "SELECT SQMEDMAGNITUD.nextval FROM DUAL";
	
	public static final String SQL_MED_MAGNITUD_UPDATE = "insert into med_magnitud "
			+ "("
			+ "ID_MAGNITUD "
			+ ",ID_EMPRESA "
			+ ",ID_COMPONENTE "
			+ ",ID_SERVICIO "
			+ ",ID_MEDIDA "
			+ ",ENTEROS "
			+ ",DECIMALES "
			+ ",FACTOR "
			+ ",VALOR "
			+ ",TIPO "
			+ ",ID_TIP_MAGNITUD "
			+ ",OBSERVACIONES "
			+ ",FEC_LEC_TERRENO "
			+ ",SEC_MAGNITUD "
			+ ",ID_EST_MAGNITUD "
			+ ",ID_TIP_CALCULO "
			+ ",ID_TIPO_CONS "
			+ ") "
			+ "values "
			+ "("
			+ "? "
			+ ",? "
			+ ",? "
			+ ",? "
			+ ",? "
			+ ",? "
			+ ",? "
			+ ",? "
			+ ",? "
			+ ",'LECTURA' "
			+ ",'4' "
			+ ",'Lectura Modulo Corte' "
			+ ",to_date(?,'ddMMyyyy hh24:mi') "
			+ ",? "
			+ ",'1' "
			+ ",? "
			+ ",'1' "
			+ ")";
}
