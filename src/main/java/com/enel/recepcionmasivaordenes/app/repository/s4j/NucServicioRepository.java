package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.dto.ServicioDTO;
import com.enel.recepcionmasivaordenes.app.entity.s4j.NucServicio;

@Repository
public interface NucServicioRepository extends JpaRepository<NucServicio, Long>{

	@Query(value="SELECT se.id_servicio AS dIdServicioH, "
			+ "se.sec_magnitud + 1 AS dSecMagnitud, "
			+ "se.id_workflow AS idWorkflowSE, "
			+ "b.id_state AS aEstadoSE "
			+ "FROM srv_electrico se, nuc_servicio a, wkf_workflow b "
			+ "WHERE a.id_empresa = ?1 "
			+ "and a.nro_servicio = ?2 "
			+ "and a.tipo = 'ELECTRICO'"
			+ "and a.id_servicio = se.id_servicio "
			+ "and se.id_workflow = b.id_workflow", nativeQuery=true)
	ServicioDTO obtenerServicio(Integer idEmpresa, Long dNroServicio);

	public static final String SQL_SRV_ELECTRICO_UPDATE = "update srv_electrico set sec_magnitud = sec_magnitud + 1 "
			+ "where id_servicio = ?";
	
	public static final String SQL_ID_REG_VERIF = "Select id_reg_verif "
			+ "from cor_reg_verif "
			+ "where id_servicio = ?";
	
	public static final String SQL_NIVEL = "Select nivel "
			+ "from cor_reg_verif "
			+ "where id_servicio = ?";
}
