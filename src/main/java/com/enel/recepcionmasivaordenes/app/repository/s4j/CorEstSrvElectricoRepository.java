package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.entity.s4j.CorEstSrvElectrico;

@Repository
public interface CorEstSrvElectricoRepository extends JpaRepository<CorEstSrvElectrico, Long> {

	@Query(value="select c.DESCRIPCION as fin "
			+ "from COR_EST_SRV_ELECTRICO a, "
			+ "cor_estado b, "
			+ "cor_estado c, "
			+ "cor_accion d "
			+ "where "
			+ "a.ID_EST_INICIAL = b.id_estado "
			+ "and a.ID_EST_FINAL = c.id_estado "
			+ "and a.id_accion = d.id_accion "
			+ "and d.ESTADO = 'A' "
			+ "and c.ESTADO = 'A' "
			+ "and b.ESTADO = 'A' "
			+ "and a.ESTADO = 'A' "
			+ "and b.DESCRIPCION = ?1 "
			+ "and d.DESCRIPCION like 'Recepcionar' || ?2 || '%' "
			+ "and ( (?2 != 'OrdenVerificacion' and ?3 = 'NoRealizado' and d.DESCRIPCION like '%NoEfectivaNoRehacerOrden') "
			+ "or (?2 != 'OrdenVerificacion' and ?3 != 'NoRealizado' and d.DESCRIPCION like '%AccionEfectiva') "
			+ " or (?2 = 'OrdenCorte' and ?3 != 'NoRealizado' and d.DESCRIPCION = 'RecepcionarOrdenCorte')"
			+ "or (?2 = 'OrdenVerificacion') "
			+ ")", nativeQuery=true)
	String obtenerEstado(String aEstadoSE, String sauxCodInterno, String sCodInternoAccion);

}
