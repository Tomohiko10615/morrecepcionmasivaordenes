package com.enel.recepcionmasivaordenes.app.repository.scom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.dto.OrdTransferDTO;
import com.enel.recepcionmasivaordenes.app.entity.scom.EorOrdTransfer;

@Repository
public interface EorOrdTransferRepository extends JpaRepository<EorOrdTransfer, Long>{

	@Query(value="select ID_ORD_TRANSFER AS lIdOrdTransfer, NRO_RECEPCIONES AS lNroRecepcion "
			+ "from schscom.EOR_ORD_TRANSFER "
			+ "where nro_orden_legacy = ? "
			+ "and COD_TIPO_ORDEN_EORDER in('SCR.01','SCR.02')", nativeQuery=true)
	OrdTransferDTO obtenerOrdTransfer(String numeroOrden);
	
	public static final String SQL_EOR_ORD_TRANSFER_UPDATE = "update schscom.EOR_ORD_TRANSFER "
			+ "set fec_operacion   = now() "
			+ ", COD_OPERACION     = ? "
			+ ", COD_ESTADO_ORDEN_ANT = COD_ESTADO_ORDEN "
			+ ", COD_ESTADO_ORDEN  = ? "
			+ ", OBSERVACIONES     = ? "
			+ "where COD_TIPO_ORDEN_EORDER in ('SCR.01','SCR.02') "
			+ "and ID_ORD_TRANSFER = ?";
	
	public static final String SQL_EOR_ORD_TRANSFER_DET_UPDATE = "update schscom.EOR_ORD_TRANSFER_DET "
			+ "set cod_error         = ? "
			+ "where ID_ORD_TRANSFER = ? "
			+ "and accion = 'RECEPCION' "
			+ "and NRO_EVENTO = ?";

}
