package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.stereotype.Repository;

@Repository
public class CorRegVerifRepository {
	
	CorRegVerifRepository() {
		super();
	}
	
	public static final String SQL_COR_REG_VERIF_CORT_UPDATE =
			"update cor_reg_verif set nivel = 1, "
			+ " FECHA_PROX_VERIF = trunc(to_date(?,'ddMMyyyy hh24:mi'))+ 3 "
			+ "where ID_REG_VERIF = ?";
	
	public static final String SQL_COR_REG_VERIF_VERI_UPDATE =
			"update cor_reg_verif set nivel = ? + 1, "
			+  " FECHA_PROX_VERIF = trunc(to_date(?,'ddMMyyyy hh24:mi'))+ 3 "
			+ "where ID_REG_VERIF = ?";
	
	public static final String SQL_COR_REG_VERIF_UPDATE =
			"update cor_reg_verif set FECHA_PROX_VERIF = to_date('01/01/2500','dd/MM/yyyy') "
			+ "where ID_REG_VERIF = ?";
	
	public static final String SQL_COR_REG_VERIF_SRV_UPDATE =
			"update cor_reg_verif set FECHA_PROX_VERIF = trunc(to_date('01/01/2500','dd/MM/yyyy')) "
			+ "where ID_SERVICIO = ?";
	
	public static final String SQL_COR_REG_VERIF_INSERT =
			"insert into cor_reg_verif( "
			+ "ID_REG_VERIF "
			+ ",ID_EMPRESA "
			+ ",ID_SERVICIO "
			+ ",NIVEL "
			+ ",FECHA_PROX_VERIF "
			+ ") "
			+ "values ( "
			+ "? "
			+ ",? "
			+ ",? "
			+ ",? "
			+ ",trunc(to_date(?,'ddMMyyyy hh24:mi'))+ ? "
			+ ")";
	
	public static final String SQL_ID_REG_VERIF = "SELECT SQREGISTROVERIFICACION.nextval FROM DUAL";
}
