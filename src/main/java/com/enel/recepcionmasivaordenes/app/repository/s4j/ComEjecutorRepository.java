package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.entity.s4j.ComEjecutor;

@Repository
public interface ComEjecutorRepository extends JpaRepository<ComEjecutor, Long> {

	@Query(value="SELECT id_ejecutor "
			+ "FROM com_ejecutor "
			+ "WHERE cod_ejecutor = ?1 "
			+ "AND id_empresa   = ?2", nativeQuery=true)
	Long obtenerIdEjecutor(String codEjecutor, Integer idEmpresa);
	

}
