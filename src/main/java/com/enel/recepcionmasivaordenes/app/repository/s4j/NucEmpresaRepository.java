package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.entity.s4j.NucEmpresa;

@Repository
public interface NucEmpresaRepository extends JpaRepository<NucEmpresa, Long> {

	@Query(value="SELECT id_empresa "
			+ "FROM nuc_empresa "
			+ "WHERE cod_partition = ?", nativeQuery=true)
	Integer obtenerIdEmpresa(String aCodPartition);

}
