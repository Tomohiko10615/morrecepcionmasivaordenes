package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.dto.TipoOrdenDTO;
import com.enel.recepcionmasivaordenes.app.entity.s4j.OrdTipoOrden;

@Repository
public interface OrdTipoOrdenRepository extends JpaRepository<OrdTipoOrden, Long>{

	@Query(value="SELECT cod_interno AS sauxCodInterno, id_tipo_orden AS dIdTipoOrdenH "
			+ "FROM ord_tipo_orden "
			+ "WHERE cod_tipo_orden = ?1 "
			+ "AND id_empresa = ?2", nativeQuery=true)
	TipoOrdenDTO obtenerTipoOrden(String aCodTipoOrden, Integer idEmpresa);

}
