package com.enel.recepcionmasivaordenes.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionmasivaordenes.app.dto.SituacionTerrenoDTO;
import com.enel.recepcionmasivaordenes.app.entity.s4j.ComSitTerreno;

@Repository
public interface ComSitTerrenoRepository extends JpaRepository<ComSitTerreno, Long> {

	@Query(value="SELECT id_sit_terreno AS dIdSitTerr, cod_interno AS sCodInternoSitTerr "
			+ "FROM com_sit_terreno "
			+ "WHERE cod_sit_terreno = ?1 "
			+ "AND id_empresa = ?2", nativeQuery=true)
	SituacionTerrenoDTO obtenerSituacionTerreno(String codSitTerr, Integer idEmpresa);
	
	public static final String SQL_ID_ACC_TERRENO = "SELECT SQACCIONTERRENO.nextval FROM DUAL";
	
	public static final String SQL_COR_LECT_ACC_TERR_INSERT = "insert into cor_lect_acc_ter "
			+ "(id_acc_terreno, id_magnitud) "
			+ "values "
			+ "(?, ?)";
	
	public static final String SQL_COR_ACC_TERRENO_INSERT = "insert into cor_acc_terreno ( "
			+ "ID_ACC_TERRENO "
			+ ",ID_ORDEN "
			+ ",ID_SERVICIO "
			+ ",ID_SIT_TERRENO_PRI "
			+ ",FECHA_EJECUCION "
			+ ",OBSERVACION "
			+ ",ID_COMPONENTE "
			+ ",ID_EMPRESA "
			+ ",ID_ACCION_REALIZADA "
			+ ",ID_SIT_TERRENO_SEC "
			+ ",ID_COBRO_ADIC "
			+ ",CODIGO_GART "
			+ ") "
			+ "values "
			+ "(? "
			+ ",? "
			+ ",? "
			+ ",? "
			+ ",to_date(?,'ddMMyyyy hh24:mi') "
			+ ",? "
			+ ",decode(?,0,NULL,?) "
			+ ",'1' "
			+ ",? "
			+ ",? "
			+ ",? "
			+ ",? "
			+ ")";

}
