package com.enel.recepcionmasivaordenes.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "cor_est_srv_electrico")
public class CorEstSrvElectrico {
	
	@Id
	@Column(name = "id_estado_inicial")
	private Long idEstadoInicial;
	
	@Column(name = "id_estado_final")
	private Long idEstadoFinal;
	
	@Column(name = "id_accion")
	private Long idAccion;
	
	private String estado;

}
