package com.enel.recepcionmasivaordenes.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "med_componente")
public class MedComponente {

	@Id
	private Long id;
	
	@Column(name = "id_ubicacion")
	private Long idUbicacion;
	
	@Column(name = "id_modelo")
	private Long idModelo;

}
