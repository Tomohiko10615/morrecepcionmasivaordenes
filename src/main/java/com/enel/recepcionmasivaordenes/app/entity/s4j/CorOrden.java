package com.enel.recepcionmasivaordenes.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "cor_orden")
public class CorOrden {
	
	@Id
	@Column(name = "id_orden")
	private Long idOrden;
	
	@Column(name = "obs_recepcion")
	private String obsRecepcion;
	
}
