package com.enel.recepcionmasivaordenes.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "wkf_workflow")
public class WkfWorkflow {

	@Id
	private Long id;
	
	@Column(name = "id_old_state")
	private String idOldState;
	
	@Column(name = "id_state")
	private String idState;
}
