package com.enel.recepcionmasivaordenes.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "cor_conf_cargo")
public class CorConfCargo {

	@Id
	@Column(name = "id_cargo")
	private Long idCargo;
	
	@Column(name = "id_valor")
	private Long idValor;
	
	@Column(name = "codigo_gart")
	private String codigoGart;
	
	@Column(name = "id_tipo_orden")
	private Long idTipoOrden;
	
	@Column(name = "id_accion_realizada")
	private Long idAccionRealizada;
	
	@Column(name = "id_sit_terreno")
	private Long idSitTerreno;
	
	@Column(name = "id_tipo_acom")
	private Long idTipoAcom;
	
	@Column(name = "id_fase")
	private Long idFase;
	
	@Column(name = "limite_inf_potencia")
	private Integer limiteInfPotencia;
	
	@Column(name = "limite_sup_potencia ")
	private Integer limiteSupPotencia;
	
	@Column(name = "tapa_con_ranura")
	private String tapaConRanura;
	
	@Column(name = "id_tarifa_base")
	private Long idTarifaBase;
	
	@Column(name = "id_sector_tipico")
	private Long idSectorTipico;
	
	@Column(name = "nivel_inicio")
	private Double nivelInicio;
	
	@Column(name = "nivel_fin")
	private Double nivelFin;
}
