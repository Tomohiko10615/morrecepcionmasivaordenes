package com.enel.recepcionmasivaordenes.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.enel.recepcionmasivaordenes.app.dto.AccionRealizadaDTO;
import com.enel.recepcionmasivaordenes.app.dto.CargoDTO;
import com.enel.recepcionmasivaordenes.app.dto.CodigoErrorDTO;
import com.enel.recepcionmasivaordenes.app.dto.ComponenteAuxDTO;
import com.enel.recepcionmasivaordenes.app.dto.ComponenteDTO;
import com.enel.recepcionmasivaordenes.app.dto.OrdTransferDTO;
import com.enel.recepcionmasivaordenes.app.dto.OrdenDTO;
import com.enel.recepcionmasivaordenes.app.dto.ServicioDTO;
import com.enel.recepcionmasivaordenes.app.dto.SituacionTerrenoDTO;
import com.enel.recepcionmasivaordenes.app.dto.TipoOrdenDTO;
import com.enel.recepcionmasivaordenes.app.model.StRegDatos;
import com.enel.recepcionmasivaordenes.app.model.StRegParametros;
import com.enel.recepcionmasivaordenes.app.repository.s4j.ComSitTerrenoRepository;
import com.enel.recepcionmasivaordenes.app.repository.s4j.CorConfCargoRepository;
import com.enel.recepcionmasivaordenes.app.repository.s4j.CorOrdenRepository;
import com.enel.recepcionmasivaordenes.app.repository.s4j.CorRegVerifRepository;
import com.enel.recepcionmasivaordenes.app.repository.s4j.MedComponenteRepository;
import com.enel.recepcionmasivaordenes.app.repository.s4j.NucServicioRepository;
import com.enel.recepcionmasivaordenes.app.repository.s4j.OrdOrdenRepository;
import com.enel.recepcionmasivaordenes.app.repository.s4j.WkfWorkflowRepository;
import com.enel.recepcionmasivaordenes.app.repository.scom.EorOrdTransferRepository;
import com.enel.recepcionmasivaordenes.app.service.ComEjecutorService;
import com.enel.recepcionmasivaordenes.app.service.ComParametrosService;
import com.enel.recepcionmasivaordenes.app.service.ComSitTerrenoService;
import com.enel.recepcionmasivaordenes.app.service.CorAccRealizadaService;
import com.enel.recepcionmasivaordenes.app.service.CorConfCargoService;
import com.enel.recepcionmasivaordenes.app.service.CorEstSrvElectricoService;
import com.enel.recepcionmasivaordenes.app.service.EorOrdTransferService;
import com.enel.recepcionmasivaordenes.app.service.MedComponenteService;
import com.enel.recepcionmasivaordenes.app.service.NucEmpresaService;
import com.enel.recepcionmasivaordenes.app.service.NucServicioService;
import com.enel.recepcionmasivaordenes.app.service.OrdObservacionService;
import com.enel.recepcionmasivaordenes.app.service.OrdOrdenService;
import com.enel.recepcionmasivaordenes.app.service.OrdTipoOrdenService;
import com.enel.recepcionmasivaordenes.app.service.UsuarioService;
import com.enel.recepcionmasivaordenes.app.service.UtilService;
import com.enel.recepcionmasivaordenes.app.service.WkfWorkflowService;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class RecepcionMasivaOrdenes {

	@Autowired
	private ComParametrosService comParametrosService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private NucServicioService nucServicioService;
	
	@Autowired
	private OrdTipoOrdenService ordTipoOrdenService;
	
	@Autowired
	private OrdOrdenService ordOrdenService;
	
	@Autowired
	private ComEjecutorService comEjecutorService;
	
	@Autowired
	private MedComponenteService medComponenteService;
	
	@Autowired
	private ComSitTerrenoService comSitTerrenoService;
	
	@Autowired
	private CorAccRealizadaService corAccRealizadaService;
	
	@Autowired
	private CorEstSrvElectricoService corEstSrvElectricoService;
	
	@Autowired
	private CorConfCargoService corConfCargoService;
	
	@Autowired
	private OrdObservacionService ordObservacionService;
	
	@Autowired
	private UtilService utilService;
	
	@Autowired
	private WkfWorkflowService wkfWorkflowService;
	
	@Autowired
	private NucEmpresaService nucEmpresaService;
	
	@Autowired
	private EorOrdTransferService eorOrdTransferService;

	private JdbcTemplate jdbcTemplateOracle;
	private JdbcTemplate jdbcTemplatePostgres;
	
	private StRegParametros stParametros = new StRegParametros();
	private StRegDatos sRegDatos;
	private Integer iRet = 0;
	private boolean existeLog = false;
	private boolean bAbortar = false;
	private FileWriter errLogWriter;
	private FileWriter procLogWriter;
	private Integer iLeidos = 0;
	private Integer iNoActualizados = 0;
	private Integer iTratados = 0;
	private Integer iNoValidados = 0;
	private String cObservacionGlobal = "";
	private String error = "";
	private String logMsj;
	
	private Connection cnxSynergia;
	private Connection cnxScom;
	
	private String vcCodErrSY002;
	private String vcDesErrSY002;
	private String vcDesErrSY000;
	private String vcCodErrSY000;
	private Long lRecep;
	private Long lRecepError;

	public void obtenerValoresEstados() {
		Long lPendRecep = comParametrosService.obtenerEstado("PRECE");
		
		if (lPendRecep == null) {
			log.error("No se encuentra configurado Estados de Transferencia Pendiente Recepción");
			System.exit(1);
		}
		
		log.info("Codigo PRECE: {}", lPendRecep);
		
		lRecep = comParametrosService.obtenerEstado("RECEP");
		
		if (lRecep == null) {
			log.error("No se encuentra configurado Estados de Transferencia Recepcionado");
			System.exit(1);
		}
		
		log.info("Codigo RECEP: {}", lRecep);
		
		lRecepError = comParametrosService.obtenerEstado("RECER");
		
		if (lRecepError == null) {
			log.error("No se encuentra configurado Estados de Transferencia Recepcionado con Error");
			System.exit(1);
		}
		
		log.info("Codigo RECER: {}", lRecepError);
		
		CodigoErrorDTO codigoErrorDTO000 = comParametrosService.obtenerEstadoError("ASY000");
		
		if (codigoErrorDTO000 == null) {
			log.error("No se encuentra configurado Codigo Error ASY000");
			System.exit(1);
		}
		
		vcCodErrSY000 = codigoErrorDTO000.getValor();
		vcDesErrSY000 = codigoErrorDTO000.getDescripcion();
		
		log.info("Codigo ASY000: {} - {}", vcCodErrSY000, vcDesErrSY000);
		
		CodigoErrorDTO codigoErrorDTO002 = comParametrosService.obtenerEstadoError("ASY002");
		
		if (codigoErrorDTO002 == null) {
			log.error("No se encuentra configurado Codigo Error ASY002");
			System.exit(1);
		}
		
		vcCodErrSY002 = codigoErrorDTO002.getValor();
		vcDesErrSY002 = codigoErrorDTO002.getDescripcion();
		
		log.info("Codigo ASY000: {} - {}", vcCodErrSY002, vcDesErrSY002);
		
		log.info("Se otuvieron correctamente todos los valores de estados");
		
	}

	public boolean validarParametros(boolean existeDirSal) {
		
		log.info("Validando empresa...");
		
		stParametros.setIEmpresa(nucEmpresaService.obtenerIdEmpresa(stParametros.getACodPartition()));
		
		if (stParametros.getIEmpresa() == null) {
			log.error("Error Validando Empresa: {}", stParametros.getACodPartition());
			return false;
		}
		
		log.info("Se obtuvo id empresa: {}", stParametros.getIEmpresa());
		
		log.info("Validando usuario...");
		
		stParametros.setIIdUsuario(usuarioService.obtenerIdUsuario(stParametros.getACodUsuario()));
		if (stParametros.getIIdUsuario() == null) {
			log.error("Error Validando Usuario: {}", stParametros.getACodUsuario());
			return false;
		}
		
		log.info("Se obtuvo id usuario: {}", stParametros.getIIdUsuario());
		
		File archivoEntrada = new File(stParametros.getAArchivoEnt());
		
		log.info("Validando archivo de entrada...");
		
		if (!archivoEntrada.exists()) {
			log.error("Error: No existe archivo entrada: {}", stParametros.getAArchivoEnt());
			return false;
		} else {
			int limiter = stParametros.getAArchivoEnt().lastIndexOf('/');
			stParametros.setADirEnt(stParametros.getAArchivoEnt().substring(0, limiter + 1));
			stParametros.setAArchivo(stParametros.getAArchivoEnt().substring(limiter + 1));
			limiter = stParametros.getAArchivo().lastIndexOf('.');
			stParametros.setAArchivoSinExt(stParametros.getAArchivo().substring(0, limiter));
			
			log.info("Path de entrada: {}", stParametros.getADirEnt());
			log.info("Archivo de entrada: {}", stParametros.getAArchivo());
			log.info("Archivo de entrada sin extensión: {}", stParametros.getAArchivoSinExt());
			log.info("Se validó la existencia del archivo de entrada");
		}
		
		if (existeDirSal) {
			
			existeLog = true;
			
			log.info("Validando directorio de salida...");
			
			File pathSalida = new File(stParametros.getADirSal());
			
			if (!pathSalida.exists()) {
				log.error("Error: No existe directorio opcional: {}", stParametros.getADirSal());
				return false;
			}
			
			log.info("Se validó la existencia del directorio de salida");
		}
		
		log.info("Se validó los parámetros con éxito");
		
		return true;
	}

	public boolean bloquearEscritura() throws IOException {
		
		String archivoLock = "/tmp/lock.txt";
		
		File file = new File(archivoLock);
		if (!file.createNewFile()) {
			log.warn("Existe otro proceso en ejecución.");
			return false;
		}

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				try {
					Files.delete(Paths.get(archivoLock));
					log.info("Archivo lock eliminado.");
					errLogWriter.close();
					procLogWriter.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		log.info("Archivo lock creado.");
		
		return true;
	}

	public boolean ejecutarProceso() throws IOException, InterruptedException, SQLException {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		//DateTimeFormatter dtfAux = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDateTime now = LocalDateTime.now();  
		log.info("Comenzó: {}", dtf.format(now));
		
		now = LocalDateTime.now();
		
		if (existeLog) {
			
			File errLog = new File(
					stParametros.getADirSal() + "/"
					+ stParametros.getAArchivoSinExt()
					+ "_" + dtf.format(now)
					+ "_err.log");
			
			if (errLog.createNewFile()) {
				log.info("Archivo log de errores: {}", errLog.toString());
			} else {
				log.error("No se puede crear archivo: {}", errLog.toString());
				System.exit(1);
			}
			
			File procLog = new File(
					stParametros.getADirSal() + "/"
					+ stParametros.getAArchivoSinExt()
					+ "_" + dtf.format(now)
					+ "_proc.log");
			
			if (procLog.createNewFile()) {
				log.info("Archivo log de proceso: {}", procLog.toString());
			} else {
				log.error("No se puede crear archivo: {}", procLog.toString());
				System.exit(1);
			}
			
			errLogWriter = new FileWriter(errLog);
			procLogWriter = new FileWriter(procLog);
			
			escribirProcLog("Procesamiento del archivo: " + stParametros.getAArchivoEnt());
			
			escribirProcLog("Comenzó: " + dtf.format(now));
			
			FileReader reader = new FileReader(stParametros.getAArchivoEnt());
			try (BufferedReader archivoEntReader = new BufferedReader(reader)) {
				String line = archivoEntReader.readLine();

				while(line != null) {
					boolean procesada = procesarLinea(line);
					if (!procesada) {
						break;
					}
					line = archivoEntReader.readLine();
				}
			}
			
			log.info("Moviendo archivo de salida...");
			
			if (stParametros.getADirSal() != null && bAbortar == false) {

				String comando = String.format("mv %s %s ;",stParametros.getAArchivoEnt(), stParametros.getADirSal());
				log.info(comando);
				
				Process process = Runtime.getRuntime().exec(new String[]{"bash","-c",comando});
				process.waitFor();
				
				if (process.exitValue() != 0) {
					log.error("No se pudo mover el archivo: {}", comando);
					return false;
				}
			}
			
			log.info("Archivo de salida movido con éxito...");
			
			now = LocalDateTime.now();
			
			logMsj = String.format("Finalizo: %s", dtf.format(now));
			escribirProcLog(logMsj);
			
			logMsj = String.format("Leidos: %d", iLeidos);
			escribirProcLog(logMsj);
			
			logMsj = String.format("Proceso: %d ordenes", iTratados);
			escribirProcLog(logMsj);
			
			logMsj = String.format("No validados: %d", iNoValidados);
			escribirProcLog(logMsj);
			
			logMsj = String.format("No actualizados: %d", iNoActualizados);
			escribirProcLog(logMsj);
		}
		
		return !bAbortar;
	}

	private boolean procesarLinea(String line) throws IOException, SQLException, InterruptedException {
		iLeidos++;
		
		log.info("Procesando línea: {}", iLeidos);
		
		int paso = separarValidar(line);
		
		log.info("Error: {}", error);
		log.info("Paso: {}", paso);
		log.info("Codigo Interno: {}", sRegDatos.getSCodInternoAccion());
			
		if (error.length() == 0
				|| (paso >= 20)
				&& !sRegDatos.getSCodInternoAccion().equals("NoRealizado")) {
			log.info(" SE = {}",sRegDatos.getDNroServicio());
			if ((paso >= 20) && !sRegDatos.getSCodInternoAccion().equals("NoRealizado")) {
				logMsj = String.format("SIN CARGO pero ACTUALIZADO (%s): %s| %s", paso, error, line);
                escribirErrLog(logMsj);
            }
			log.info("RecepcionMasiva del estado: {}", sRegDatos.getSNewEstadoSE());
			
			actualizarOrden();
			log.info("Error: {}", error);
			if (error.length() > 0) {
	               ++iNoActualizados;
	               logMsj = String.format(
	            		   "Error en registro %s - Servicio (%.0d) : %s | %s",
	            		   iLeidos.toString(), sRegDatos.getDNroServicio(), error, line);
	               
	               escribirErrLog(logMsj);
	               if (error.indexOf("ABORTAR") >= 0) {
	                   log.info("Error en registro {}: SE CANCELA POR ERROR EN LA BASE",iLeidos);
	                   bAbortar = true;
	                   return false;
	                  }
	               cnxSynergia.rollback();
	           }
	           else {
	        	   log.info("Commit...");
	               ++iTratados;
	               cnxSynergia.commit();
	           }
		} else {
			log.info("Orden con error");

        	log.info(" SE No procesado = {} ",sRegDatos.getDNroServicio());
        	++iNoValidados;
        	
         	logMsj = String.format("Error(paso:%d): %s| %s ", paso, error, line);
         	escribirErrLog(logMsj);
         	
         	if (error.indexOf("ABORTAR") >= 0) {
                 log.info("Error en registro {}: SE CANCELA POR ERROR DE FORMATO | {}",iLeidos, line);
         	}
            
		}
		
		actualizarOrdenTransferencia();
     	
     	cnxScom.commit();
		
		return true;
	}

	private void actualizarOrdenTransferencia() throws SQLException {
		
		log.info("Actualizando orden transf...");
		
		String cObservacion;

        String cCodOperacion;
        
        Long lEstadoOrden;
        
        int result;
        
        log.info(sRegDatos.getNumeroOrden());
       
		OrdTransferDTO ordTransferDTO = eorOrdTransferService.obtenerOrdTransfer(sRegDatos.getNumeroOrden());
		
		if (ordTransferDTO == null) {
			error = "Error: Leyendo Datos de Transferencia";
			return;
		}
		
		log.info(ordTransferDTO.getLIdOrdTransfer().toString());
		log.info(ordTransferDTO.getLNroRecepcion().toString());
		
		if (error.length() == 0) {
			cCodOperacion = vcCodErrSY000;
			cObservacion = vcDesErrSY000;
			lEstadoOrden = lRecep;
		} else {
			cCodOperacion = vcCodErrSY002;
			if (cObservacionGlobal.length() != 0) {
				cObservacion = cObservacionGlobal;
			} else {
				cObservacion = vcDesErrSY002;
			}
			lEstadoOrden = lRecepError;
		}
		
		log.info(cCodOperacion);
		log.info(cObservacion);
		log.info(lEstadoOrden.toString());
		
		String sqlEorOrdTransferUpdate = EorOrdTransferRepository.SQL_EOR_ORD_TRANSFER_UPDATE;
		
		result = jdbcTemplatePostgres.update(sqlEorOrdTransferUpdate,
				cCodOperacion,
				lEstadoOrden,
				cObservacion,
				ordTransferDTO.getLIdOrdTransfer());
		
		if (result == 0) {
			error = "Error: Actualiza EOR_ORD_TRANSFER .";
			
			return;
		}
		
		String sqlEorOrdTransferDetUpdate = EorOrdTransferRepository.SQL_EOR_ORD_TRANSFER_DET_UPDATE;
		
		result =jdbcTemplatePostgres.update(sqlEorOrdTransferDetUpdate,
				cCodOperacion,
				ordTransferDTO.getLIdOrdTransfer(),
				ordTransferDTO.getLNroRecepcion());
		
		if (result == 0) {
			error = "Error: Actualiza EOR_ORD_TRANSFER DET .";
			log.info(error);
			return;
		}
		
		log.info("Se actualizó las ordenes en EorOrdTransfer");
	
	}
	
	private void actualizarOrden() throws SQLException, InterruptedException {
		log.info("Actualizando orden...");
		
		Float valorLect;
		
		if (sRegDatos.getLecturas().length() > 0) {
		    valorLect = Float.parseFloat(sRegDatos.getLecturas());
		    if (valorLect < 0)
		        valorLect = 0F;
		}
		 else
		    valorLect = -1F;
		
		int updated = 1;
		int result;
		error = "";
		
		log.info("Actualizando OrdOrden...");
		
		String sqlOrdOrdenUpdate = OrdOrdenRepository.SQL_ORD_ORDEN_UPDATE;

		result = jdbcTemplateOracle.update(sqlOrdOrdenUpdate, sRegDatos.getFechaEjec(), sRegDatos.getDIdOrden());
		
		updated = updated * result;
		
		log.info("Actualizando workflow de la orden...");
		
		String sqlWorkFlowOrdenUpdate = WkfWorkflowRepository.SQL_WFK_WORKFLOW_ORDEN_UPDATE;
		
		result = jdbcTemplateOracle.update(sqlWorkFlowOrdenUpdate, "Finalizada", sRegDatos.getDIdWorkflowOrd());
		
		updated = updated * result;
		
		log.info("Actualizando CorOrden...");
		
		String sqlCorOrdenUpdate = CorOrdenRepository.SQL_COR_ORDEN_UPDATE;
		
		result = jdbcTemplateOracle.update(sqlCorOrdenUpdate,
				sRegDatos.getDIdEjecutor(),
				sRegDatos.getObservacion(),
				sRegDatos.getFechaEjec(),
				sRegDatos.getDIdOrden());
		
		updated = updated * result;
		
		log.info("Actualiza srv_electrico...");
		
		String sqlSrvElectricoUpdate = NucServicioRepository.SQL_SRV_ELECTRICO_UPDATE;
		
		result = jdbcTemplateOracle.update(sqlSrvElectricoUpdate, sRegDatos.getDIdServicio());
		
		updated = updated * result;
		
		log.info("Actualiza wkf servicio electrico...");
		
		String sqlWorkflowSEUpdate = WkfWorkflowRepository.SQL_WFK_WORKFLOW_SE_UPDATE;
		
		result = jdbcTemplateOracle.update(sqlWorkflowSEUpdate, sRegDatos.getSNewEstadoSE(), sRegDatos.getDIdWorkflowSE());
		
		updated = updated * result;
		
		log.info("Inserta Cargo FAC_COBRO_ADIC...");
		
		String sqlCorAccTerrenoInsert = ComSitTerrenoRepository.SQL_COR_ACC_TERRENO_INSERT;
		String sqlIdAccTerreno = ComSitTerrenoRepository.SQL_ID_ACC_TERRENO;
		
		Long idAccTerreno = jdbcTemplateOracle.queryForObject(sqlIdAccTerreno, Long.class);
		
		if (idAccTerreno == null) updated = 0;
		
		if (sRegDatos.getIdCodValor() > 0 && sRegDatos.getIdCargo() > 0) {
			log.info("Existe cargo");
			log.info("Actualizando cobro adic...");
			String sqlIdCobroAdic = CorConfCargoRepository.SQL_ID_COBRO_ADIC;
			Long idCobroAdic = jdbcTemplateOracle.queryForObject(sqlIdCobroAdic, Long.class);
			if (idCobroAdic == null) updated = 0;
			String sqlFacCobroAdicInsert = CorConfCargoRepository.SQL_FAC_COBRO_ADIC_INSERT;
			result = jdbcTemplateOracle.update(sqlFacCobroAdicInsert,
					idCobroAdic,
					sRegDatos.getIdCargo(),
					stParametros.getIEmpresa(),
					sRegDatos.getIdCodValor(),
					sRegDatos.getFechaEjec(),
					sRegDatos.getDIdServicio()
					);
			
			updated = updated * result;
			
			log.info("inserta Cor_acc_terreno...");
			
			if (sRegDatos.getDIdSitTerr2() > 0) {
				log.info("Existe situacion terreno sec");
				log.info("Actualizando acc terreno...");
				result = jdbcTemplateOracle.update(sqlCorAccTerrenoInsert,
						idAccTerreno,
						sRegDatos.getDIdOrden(),
						sRegDatos.getDIdServicio(),
						sRegDatos.getDIdSitTerr1(),
						sRegDatos.getFechaEjec(),
						sRegDatos.getObservacion(),
						sRegDatos.getDIdComponente(),
						sRegDatos.getDIdComponente(),
						sRegDatos.getDIdAccRealizada(),
						sRegDatos.getDIdSitTerr2(),
						idCobroAdic,
						sRegDatos.getACodGart()
						);
				updated = updated * result;
			} else {
				log.info("No existe situacion terreno sec");
				log.info("Actualizando acc terreno...");
				result = jdbcTemplateOracle.update(sqlCorAccTerrenoInsert,
						idAccTerreno,
						sRegDatos.getDIdOrden(),
						sRegDatos.getDIdServicio(),
						sRegDatos.getDIdSitTerr1(),
						sRegDatos.getFechaEjec(),
						sRegDatos.getObservacion(),
						sRegDatos.getDIdComponente(),
						sRegDatos.getDIdComponente(),
						sRegDatos.getDIdAccRealizada(),
						null,
						idCobroAdic,
						sRegDatos.getACodGart());
				updated = updated * result;
			}
		} else {
			log.info("No existe cargo...");
			if (sRegDatos.getDIdSitTerr2() > 0) {
				log.info("Existe situacion terreno sec");
				log.info("Actualizando acc terreno...");
				result = jdbcTemplateOracle.update(sqlCorAccTerrenoInsert,
						idAccTerreno,
						sRegDatos.getDIdOrden(),
						sRegDatos.getDIdServicio(),
						sRegDatos.getDIdSitTerr1(),
						sRegDatos.getFechaEjec(),
						sRegDatos.getObservacion(),
						sRegDatos.getDIdComponente(),
						sRegDatos.getDIdComponente(),
						sRegDatos.getDIdAccRealizada(),
						sRegDatos.getDIdSitTerr2(),
						null,
						null
						);
				updated = updated * result;
			} else {
				log.info("No existe situacion terreno sec");
				log.info("Actualizando acc terreno...");
				result = jdbcTemplateOracle.update(sqlCorAccTerrenoInsert,
						idAccTerreno,
						sRegDatos.getDIdOrden(),
						sRegDatos.getDIdServicio(),
						sRegDatos.getDIdSitTerr1(),
						sRegDatos.getFechaEjec(),
						sRegDatos.getObservacion(),
						sRegDatos.getDIdComponente(),
						sRegDatos.getDIdComponente(),
						sRegDatos.getDIdAccRealizada(),
						null,
						null,
						null);
				updated = updated * result;
			}
		}
		
		
		String sqlIdMagnitud = MedComponenteRepository.SQL_ID_MAGNITUD;
		
		Long idMagnitud = jdbcTemplateOracle.queryForObject(sqlIdMagnitud, Long.class);
		
		if (idMagnitud == null) updated = 0;
		
		if (valorLect > 0 && sRegDatos.getDIdComponente() > 0) {
			log.info("Existe lectura y componente");
			log.info("Actualizando magnitud...");
			String sqlMedMagnitudUpdate = MedComponenteRepository.SQL_MED_MAGNITUD_UPDATE;
			result = jdbcTemplateOracle.update(sqlMedMagnitudUpdate,
					idMagnitud,
					stParametros.getIEmpresa(),
					sRegDatos.getDIdComponente(),
					sRegDatos.getDIdServicio(),
					sRegDatos.getDIdMedida(),
					sRegDatos.getDEnteros(),
					sRegDatos.getDDecimales(),
					sRegDatos.getDFactor(),
					valorLect,
					sRegDatos.getFechaEjec(),
					sRegDatos.getDsecMagnitud(),
					sRegDatos.getDIdTipoCalculo());
			
			updated = updated * result;
			
			log.info("Actualizando acc terreno...");
			
			String sqlCorLectAccTerrInsert = ComSitTerrenoRepository.SQL_COR_LECT_ACC_TERR_INSERT;
			
			result = jdbcTemplateOracle.update(sqlCorLectAccTerrInsert,
					idAccTerreno,
					idMagnitud);
			
			updated = updated * result;
		}
		
		if (updated == 0) {
			error = "Error: recepcionar(Ejecutar Execute II).";
		}
		
		if (sRegDatos.getDIdTipoOrden() == 10 || sRegDatos.getDIdTipoOrden() == 11) {
			log.info("Son ordenes 10 o 11");
			String sqlIdRegVerif = NucServicioRepository.SQL_ID_REG_VERIF;
			String sqlNivel = NucServicioRepository.SQL_NIVEL;
			
			Long idRegVerif = jdbcTemplateOracle.queryForObject(sqlIdRegVerif, Long.class, sRegDatos.getDIdServicio());
			
			if (idRegVerif == null) {
				error = error + "Error: Leyendo reg_verif .";
				return;
			}
			
			Integer nivel = jdbcTemplateOracle.queryForObject(sqlNivel, Integer.class, sRegDatos.getDIdServicio());
			
			if (nivel == null) {
				error = error + "Error: Leyendo nivel .";
				return;
			}
			
			String sqlCorRegVerifCortUpdate = CorRegVerifRepository.SQL_COR_REG_VERIF_CORT_UPDATE;
			String sqlCorRegVerifVeriUpdate = CorRegVerifRepository.SQL_COR_REG_VERIF_VERI_UPDATE;
			String sqlCorRegVerifUpdate = CorRegVerifRepository.SQL_COR_REG_VERIF_UPDATE;
			
			if (idRegVerif > 1) {
				log.info("Existe id reg verif");
				if (sRegDatos.getACodTipoOrden().equals("CORT")
						&& !sRegDatos.getSCodInternoAccion().equals("NoRealizado")) {
					log.info("Son cortes y no son NoRealizado");
					log.info("Actualizando cor reg verif...");
					result = jdbcTemplateOracle.update(sqlCorRegVerifCortUpdate,
							sRegDatos.getFechaEjec(),
							idRegVerif);
					if (result == 0) {
						error = error + "Error: Actualizando reg_verif .";
						return;
					}
					
				} else if (sRegDatos.getACodTipoOrden().equals("VERI")
						&& !sRegDatos.getSCodInternoAccion().equals("NoRealizado")) {
					log.info("Son verificaciones y no son NoRealizado");
					log.info("Actualizando cor reg verif...");
					result = jdbcTemplateOracle.update(sqlCorRegVerifVeriUpdate,
							nivel,
							sRegDatos.getFechaEjec(),
							idRegVerif);
					if (result == 0) {
						error = error + "Error: Actualizando reg_verif .";
						return;
					}
				} else {
					log.info("Son otros casos");
					log.info("Actualizando cor reg verif...");
					result = jdbcTemplateOracle.update(sqlCorRegVerifUpdate, idRegVerif);
					if (result == 0) {
						error = error + "Error: Actualizando reg_verif .";
						return;
					}
				}
			} else {
				log.info("No existe id reg verif");
				sqlIdRegVerif = CorRegVerifRepository.SQL_ID_REG_VERIF;
				idRegVerif = jdbcTemplateOracle.queryForObject(sqlIdRegVerif, Long.class);
				
				if (idRegVerif == null) {
					error = error + "Leyendo secuencia reg_verif ";
					return;
				}
				
				String sqlCorRegVerifInsert = CorRegVerifRepository.SQL_COR_REG_VERIF_INSERT;
				if (sRegDatos.getACodTipoOrden().equals("CORT")
						&& !sRegDatos.getSCodInternoAccion().equals("NoRealizado")) {
					log.info("Son cortes y no son NoRealizado");
					log.info("Creando registro cor reg verif...");
					result = jdbcTemplateOracle.update(sqlCorRegVerifInsert,
							idRegVerif,
							stParametros.getIEmpresa(),
							sRegDatos.getDIdServicio(),
							1,
							sRegDatos.getFechaEjec(),
							3);
					if (result == 0) {
						error = error + "Error: Actualizando reg_verif .";
						return;
					}
				} else if (sRegDatos.getACodTipoOrden().equals("VERI")
						&& !sRegDatos.getSCodInternoAccion().equals("NoRealizado")) {
					log.info("Son verificaciones y no son NoRealizado");
					log.info("Creando registro cor reg verif...");
					result = jdbcTemplateOracle.update(sqlCorRegVerifInsert,
							idRegVerif,
							stParametros.getIEmpresa(),
							sRegDatos.getDIdServicio(),
							2,
							sRegDatos.getFechaEjec(),
							3);
					if (result == 0) {
						error = error + "Error: Insert Reg_verif. ";
						return;
					}
				} else if (sRegDatos.getACodTipoOrden().equals("VERI")
						&& sRegDatos.getSCodInternoAccion().equals("NoRealizado")) {
					log.info("Son verificaciones y son NoRealizado");
					log.info("Creando registro cor reg verif...");
					result = jdbcTemplateOracle.update(sqlCorRegVerifInsert,
							idRegVerif,
							stParametros.getIEmpresa(),
							sRegDatos.getDIdServicio(),
							1,
							sRegDatos.getFechaEjec(),
							0);
					if (result == 0) {
						error = error + "Error: Insert Reg_verif. ";
						return;
					}
				}
			}
		} else if (sRegDatos.getDIdTipoOrden() == 12
				&& !sRegDatos.getSCodInternoAccion().equals("NoRealizado")) {
			log.info("Son ordenes 12 y no son Norealizado");
			log.info("Actualizando cor reg verif...");
			String sqlCorRegVerifSrvUpdate = CorRegVerifRepository.SQL_COR_REG_VERIF_SRV_UPDATE;
			result = jdbcTemplateOracle.update(sqlCorRegVerifSrvUpdate, sRegDatos.getDIdServicio());
			if (result == 0) {
				error = error + "Error: Actualizando reg_verif .";
				return;
			}
		}
	}

	private int separarValidar(String line) {
		String[] cols = (line).split(";", -1);
		
		Long nroServicio;
		
		try {
			nroServicio = Long.parseLong(cols[3]);
		} catch (NumberFormatException e) {
			log.info("Formato Nro de servicio incorrecto");
			nroServicio = null;
		}
		
		if (cols.length != 13 || nroServicio == null) {
			log.error("ABORTAR: Mal formato.");
			return 1;
		} else {
			sRegDatos = new StRegDatos();
			sRegDatos.setNumeroOrden(cols[0]);
			sRegDatos.setACodTipoOrden(cols[1]);
			sRegDatos.setCodEjecutor(cols[2]);
			sRegDatos.setDNroServicio(nroServicio);
			sRegDatos.setNroComponente(cols[4]);
			sRegDatos.setCodMarca(cols[5]);
			sRegDatos.setCodModelo(cols[6]);
			sRegDatos.setCodSitTerr1(cols[7]);
			sRegDatos.setCodSitTerr2(cols[8]);
			sRegDatos.setCodAccRealizada(cols[9]);
			sRegDatos.setFechaEjec(cols[10]);
			sRegDatos.setObservacion(cols[11]);
			sRegDatos.setLecturas(cols[12]);
			
			log.info("Se obtuvo los campos correctamente");
		}
		
		log.info("Validando campos obtenidos...");
		
		return validarCampos();
	}

	private int validarCampos() {
		
		int paso = 0;
		error = "";
		
		int cantDias = jdbcTemplateOracle.queryForObject("select sysdate - to_date(?,'ddMMyyyy hh24:mi')\n"
				+ "   from dual", Integer.class, sRegDatos.getFechaEjec());
		
		log.info("Validando servicio eléctrico...");
		
		ServicioDTO servicioDTO = 
				nucServicioService.obtenerServicio(stParametros.getIEmpresa(), sRegDatos.getDNroServicio());
		
		if (servicioDTO == null) {
			paso = 1;
			cObservacionGlobal = "Servicio electrico no existe";
			error = error + " " + cObservacionGlobal + "\n";
			log.info(cObservacionGlobal);
			log.info("id_empresa: {}", stParametros.getIEmpresa());
			return paso;
		}
		
		log.info("Se encontró el servicio eléctrico");
		
		log.info("Validando tipo orden...");
		
		TipoOrdenDTO tipoOrdenDTO = 
				ordTipoOrdenService.obtenerTipoOrden(
						sRegDatos.getACodTipoOrden(), stParametros.getIEmpresa());
		
		if (tipoOrdenDTO == null) {
			paso = 2;
			cObservacionGlobal = "Cod. Tipo Orden no Existe (" + sRegDatos.getACodTipoOrden() + ")";
			error = error + " " + cObservacionGlobal + "\n";
			log.info(cObservacionGlobal);
			return paso;
		}
		
		log.info("Se encontró el tipo de orden");
		
		log.info("Validando orden...");
		
		OrdenDTO ordenDTO = ordOrdenService.obtenerOrden(
				tipoOrdenDTO.getDIdTipoOrdenH(),
				sRegDatos.getNumeroOrden());
		
		if (ordenDTO == null) {
			paso = 3;
			cObservacionGlobal = "Orden no Existe (" + sRegDatos.getNumeroOrden() + ")";
			error = error + " " + cObservacionGlobal + "\n";
			log.info(cObservacionGlobal);
			log.info(sRegDatos.getFechaEjec());
			log.info(tipoOrdenDTO.getDIdTipoOrdenH().toString());
			log.info(sRegDatos.getNumeroOrden());
			return paso;
		}
		
		log.info("Se encontró la orden");
		
		if (!ordenDTO.getAEstadoOrden().equals("Emitida")
				&& !ordenDTO.getAEstadoOrden().equals("Creada")) {
			paso = 4;
			cObservacionGlobal = "Orden no esta emitida o creada. Estado Actual (" + ordenDTO.getAEstadoOrden() + ")";
			error = error + " " + cObservacionGlobal + "\n";
			log.info(cObservacionGlobal);
			return paso;
		}
		
		log.info("La orden está en estado emitida o creada");
		
		log.info("Validando ejecutor...");
		
		Long dIdEjecutor = comEjecutorService.obtenerIdEjecutor(sRegDatos.getCodEjecutor(), stParametros.getIEmpresa());
		
		if (dIdEjecutor == null) {
			paso = 5;
			cObservacionGlobal = "Ejecutor no Existe (" + sRegDatos.getCodEjecutor() + ")";
			error = error + " " + cObservacionGlobal + "\n";
			log.info(cObservacionGlobal);
			return paso;
		}
		
		log.info("Se encontró el ejecutor");
		
		ComponenteDTO componenteDTO = null;
		
		if (Long.parseLong(sRegDatos.getNroComponente()) != 0) {
			log.info("Validando componente...");
			
			componenteDTO = medComponenteService.obtenerComponente(
					servicioDTO.getDIdServicioH(),
					sRegDatos.getNroComponente(),
					sRegDatos.getCodModelo(),
					sRegDatos.getCodMarca());
		}
		
		log.info("Validando situación terreno 1...");
		
		SituacionTerrenoDTO situacionTerrenoDTO1 =
				comSitTerrenoService.obtenerSituacionTerreno(sRegDatos.getCodSitTerr1(), stParametros.getIEmpresa());
		
		if (situacionTerrenoDTO1 == null) {
			paso = 8;
			cObservacionGlobal = "Situacion Terreno 1 no Existe (" + sRegDatos.getCodSitTerr1() + ")";
			error = error + " " + cObservacionGlobal + "\n";
			log.info(cObservacionGlobal);
			return paso;
		}
		
		SituacionTerrenoDTO situacionTerrenoDTO2 = null;
		
		if (!sRegDatos.getCodSitTerr2().equals("")) {
		
			log.info("Validando situación terreno 2...");
			
			situacionTerrenoDTO2 =
					comSitTerrenoService.obtenerSituacionTerreno(sRegDatos.getCodSitTerr2(), stParametros.getIEmpresa());
			
			if (situacionTerrenoDTO2 == null) {
				paso = 9;
				cObservacionGlobal = "Situacion Terreno 2 no Existe (" + sRegDatos.getCodSitTerr2() + ")";
				error = error + " " + cObservacionGlobal + "\n";
				log.info(cObservacionGlobal);
				return paso;
			}
		}
		
		log.info("Validando acción realizada...");
		
		AccionRealizadaDTO accionRealizadaDTO =
				corAccRealizadaService.obtenerAccionRealizada(
						sRegDatos.getCodAccRealizada(), stParametros.getIEmpresa());
		
		if (accionRealizadaDTO == null) {
			paso = 10;
			cObservacionGlobal = "Acc.Realizada no Existe (" + sRegDatos.getCodAccRealizada() + ")";
			error = error + " " + cObservacionGlobal + "\n";
			log.info(cObservacionGlobal);
			return paso;
		}
		
		log.info("Validando estado actual del servicio...");
		
		String sNewEstadoSE =
				corEstSrvElectricoService.obtenerEstado(
				servicioDTO.getAEstadoSE(),
				tipoOrdenDTO.getSauxCodInterno(),
				accionRealizadaDTO.getSCodInternoAccion());
		
		if (sNewEstadoSE == null) {
			paso = 11;
			cObservacionGlobal = "Estado invalido del Serv.Elect. (" + servicioDTO.getAEstadoSE() + ")"
					+ ") o accion no Valida (" + sRegDatos.getCodAccRealizada() + ")";
			error = error + " " + cObservacionGlobal + "\n";
			log.info(cObservacionGlobal);
			log.info(servicioDTO.getAEstadoSE());
			log.info(tipoOrdenDTO.getSauxCodInterno());
			log.info(accionRealizadaDTO.getSCodInternoAccion());
			return paso;
		}
		
		log.info("Extrae el cargo y codigo gart");
		
		ComponenteAuxDTO componenteAuxDTO =
				medComponenteService.obtenerComponenteAux(servicioDTO.getDIdServicioH());
		
		if (componenteAuxDTO == null) {
			paso = 23;
		}
		
		log.info("OBTIENE EL CARGO PARA REPO Y CORTE");
		
		boolean err = false;
		String errH = "";
		
		CargoDTO cargoDTO = null;
		if (componenteAuxDTO != null) {
			if (componenteAuxDTO.getIdTipoAcometida() > 0) {
				log.info("Id tipo acometidad existe");
				if (tipoOrdenDTO.getDIdTipoOrdenH() == 10 || tipoOrdenDTO.getDIdTipoOrdenH() == 12) {
					log.info("Se obtendrá el cargo para tipos de orden 10 0 12");
					cargoDTO = corConfCargoService.obtenerCargo(
							tipoOrdenDTO.getDIdTipoOrdenH(),
							accionRealizadaDTO.getDIdAccRealizada(),
							situacionTerrenoDTO1.getDIdSitTerr(),
							componenteAuxDTO.getIdTipoAcometida(),
							componenteAuxDTO.getIdFase(),
							componenteAuxDTO.getPotenciaContratada(),
							componenteAuxDTO.getTapaRanura(),
							componenteAuxDTO.getIdTarifa(),
							componenteAuxDTO.getSectorTipico());
					
					if (cargoDTO == null) {
						log.info("No se pudo obtener el cargo para tipos de orden 10 0 12");
						err = true;
						paso = 21;
					}
				} else if (tipoOrdenDTO.getDIdTipoOrdenH() == 11) {
					log.info("Se obtendrá el cargo para tipo de orden 11");
					cargoDTO = corConfCargoService.obtenerCargoAux(
							tipoOrdenDTO.getDIdTipoOrdenH(),
							accionRealizadaDTO.getDIdAccRealizada(),
							situacionTerrenoDTO1.getDIdSitTerr(),
							componenteAuxDTO.getIdTipoAcometida(),
							componenteAuxDTO.getIdFase(),
							componenteAuxDTO.getPotenciaContratada(),
							ordenDTO.getDNivel(),
							componenteAuxDTO.getIdTarifa(),
							componenteAuxDTO.getSectorTipico());
					
					if (cargoDTO == null) {
						log.info("No se pudo obtener el cargo para tipo de orden 11");
						err = true;
						paso = 21;
						log.info("No se pudo obtener el cago para la situación terreno 1");
					}
				}
				
				if (err && situacionTerrenoDTO2 != null) {
					log.info("Se intentará obtener para la situación terreno 2");
					if (tipoOrdenDTO.getDIdTipoOrdenH() == 10 || tipoOrdenDTO.getDIdTipoOrdenH() == 12) {
						log.info("Se obtendrá el cargo para tipos de orden 10 o 12");
						log.info(tipoOrdenDTO.getDIdTipoOrdenH().toString());
						log.info(accionRealizadaDTO.getDIdAccRealizada().toString());
						log.info(situacionTerrenoDTO2.getDIdSitTerr().toString());
						log.info(componenteAuxDTO.getIdTipoAcometida().toString());
						log.info(componenteAuxDTO.getIdFase().toString());
						log.info(componenteAuxDTO.getPotenciaContratada().toString());
						log.info(componenteAuxDTO.getTapaRanura());
						log.info(componenteAuxDTO.getIdTarifa().toString());
						log.info(componenteAuxDTO.getSectorTipico().toString());
						cargoDTO = corConfCargoService.obtenerCargo(
								tipoOrdenDTO.getDIdTipoOrdenH(),
								accionRealizadaDTO.getDIdAccRealizada(),
								situacionTerrenoDTO2.getDIdSitTerr(),
								componenteAuxDTO.getIdTipoAcometida(),
								componenteAuxDTO.getIdFase(),
								componenteAuxDTO.getPotenciaContratada(),
								componenteAuxDTO.getTapaRanura(),
								componenteAuxDTO.getIdTarifa(),
								componenteAuxDTO.getSectorTipico());
						
						if (cargoDTO == null) {
							log.info("No se pudo obtener el cargo para tipos de orden 10 o 12");
							paso = 22;
						}
					} else if (tipoOrdenDTO.getDIdTipoOrdenH() == 11) {
						log.info("Se obtendrá el cargo para tipo de orden 11");
						cargoDTO = corConfCargoService.obtenerCargoAux(
								tipoOrdenDTO.getDIdTipoOrdenH(),
								accionRealizadaDTO.getDIdAccRealizada(),
								situacionTerrenoDTO2.getDIdSitTerr(),
								componenteAuxDTO.getIdTipoAcometida(),
								componenteAuxDTO.getIdFase(),
								componenteAuxDTO.getPotenciaContratada(),
								ordenDTO.getDNivel(),
								componenteAuxDTO.getIdTarifa(),
								componenteAuxDTO.getSectorTipico());
						
						if (cargoDTO == null) {
							log.info("No se pudo obtener el cargo para tipo de orden 11");
							paso = 22;
						}
					}
				}
			} 
		}
		
		if (paso >= 20 && !accionRealizadaDTO.getSCodInternoAccion().equals("NoRealizado")) {
			errH = errH + "Sin Cargo";
		}
		String strAux = null;
		if (componenteAuxDTO != null) {
			strAux = String.format(". (Acom-Fase-Tar-Ran-Pot-SecTip): %x-%x-%x-%s-%x-%x-%x(%s)-%x(%s)"
		              ,componenteAuxDTO.getIdTipoAcometida()
		              ,componenteAuxDTO.getIdFase()
		              ,componenteAuxDTO.getIdTarifa()
		              ,componenteAuxDTO.getTapaRanura()
		              ,componenteAuxDTO.getPotenciaContratada()
		              ,componenteAuxDTO.getSectorTipico()
		              ,situacionTerrenoDTO1.getDIdSitTerr()
		              ,sRegDatos.getCodSitTerr1()
		              ,accionRealizadaDTO.getDIdAccRealizada()
		              ,sRegDatos.getCodAccRealizada());
		} else {
			strAux = String.format(". (Acom-Fase-Tar-Ran-Pot-SecTip): %x-%x-%x-%s-%x-%x-%x(%s)-%x(%s)"
		              ,0L
		              ,0L
		              ,0L
		              ,0L
		              ,0
		              ,0L
		              ,situacionTerrenoDTO1.getDIdSitTerr()
		              ,sRegDatos.getCodSitTerr1()
		              ,accionRealizadaDTO.getDIdAccRealizada()
		              ,sRegDatos.getCodAccRealizada());
		}
		
		
		if (sNewEstadoSE.length() == 0) {
			paso = 13;
			error = error + " " + String.format("%s %s%s%s%s%s",error,"Estado invalido del servicio (", servicioDTO.getAEstadoSE(), ") para la accion (",sRegDatos.getCodAccRealizada(),")" );
		}
		
		error = error + errH;
		
		if (paso >= 20 && !accionRealizadaDTO.getSCodInternoAccion().equals("NoRealizado")) {
			error = error + strAux;
		}
		
		sRegDatos.setDIdTipoOrden(tipoOrdenDTO.getDIdTipoOrdenH());
		sRegDatos.setDIdOrden(ordenDTO.getDIdOrdenH());
		sRegDatos.setDIdWorkflowOrd(ordenDTO.getDIdWorkflowOrd());
		sRegDatos.setDsecMagnitud(servicioDTO.getDSecMagnitud());
		sRegDatos.setDIdServicio(servicioDTO.getDIdServicioH());
		sRegDatos.setDIdWorkflowSE(servicioDTO.getIdWorkflowSE());
		sRegDatos.setDIdEjecutor(dIdEjecutor);
		if (componenteDTO == null) {
			sRegDatos.setNroComponente("");
			sRegDatos.setDIdComponente(0L);
		} else {
			sRegDatos.setDIdComponente(componenteDTO.getDIdComponente());
			sRegDatos.setDIdMedida(componenteDTO.getDIdMedida()); 
			sRegDatos.setDEnteros(componenteDTO.getDEnteros());
			sRegDatos.setDDecimales(componenteDTO.getDDecimales());
			sRegDatos.setDFactor(componenteDTO.getDFactor());
			sRegDatos.setDIdTipoCalculo(componenteDTO.getDIdTipoCalculo());
		}
		
		sRegDatos.setDIdSitTerr1(situacionTerrenoDTO1.getDIdSitTerr());
		sRegDatos.setSCodInternoSitTerr1(situacionTerrenoDTO1.getSCodInternoSitTerr());
		if (situacionTerrenoDTO2 != null) {
			sRegDatos.setDIdSitTerr2(situacionTerrenoDTO2.getDIdSitTerr());
			sRegDatos.setSCodInternoSitTerr2(situacionTerrenoDTO2.getSCodInternoSitTerr());
		} else {
			sRegDatos.setDIdSitTerr2(0L);
			sRegDatos.setSCodInternoSitTerr2("");
		}
		sRegDatos.setDIdAccRealizada(accionRealizadaDTO.getDIdAccRealizada());
		sRegDatos.setSCodInternoAccion(accionRealizadaDTO.getSCodInternoAccion());
		sRegDatos.setSNewEstadoSE(sNewEstadoSE);
		if (cargoDTO != null) {
			log.info("Se encontró el cargo");
			sRegDatos.setIdCargo(cargoDTO.getIdCargo());
			sRegDatos.setIdCodValor(cargoDTO.getIdCodValor());
			sRegDatos.setACodGart(cargoDTO.getACodGart());
		} else {
			sRegDatos.setIdCargo(0L);
			sRegDatos.setIdCodValor(0L);
			sRegDatos.setACodGart("");
		}
		
		if (cantDias < -2 || cantDias > 15) {
			paso = 30;
			error = error + "Fecha de ejecucion incoherente\n";
			return paso;
		}
		
		log.info("Validación completada con éxito");
		
		return paso;
	}

	private void escribirProcLog(String logMsj) throws IOException {
		log.info(logMsj);
		procLogWriter.write(logMsj + "\n");
	}
	
	private void escribirErrLog(String logMsj) throws IOException {
		log.info(logMsj);
		procLogWriter.write(logMsj + "\n");
	}

}
