package com.enel.recepcionmasivaordenes.app.dto;

public interface OrdenDTO {
	
	Long getDIdOrdenH();
	Long getDIdWorkflowOrd();
	String getAEstadoOrden();
	Integer getDNivel();
	String getCFecCreacion();
	Float getCantDias();
}
