package com.enel.recepcionmasivaordenes.app.dto;

public interface CargoDTO {

	Long getIdCargo();
	Long getIdCodValor();
	String getACodGart();
}
