package com.enel.recepcionmasivaordenes.app.dto;

public interface ComponenteDTO {
	
	Long getDIdComponente();
	Long getDIdMedida();
	Double getDEnteros();
	Double getDDecimales();
	Double getDFactor();
	Long getDIdTipoCalculo();
	
}
