package com.enel.recepcionmasivaordenes.app.dto;

public interface ComponenteAuxDTO {

	Long getIdTipoAcometida();
	Long getIdFase();
	Long getIdTarifa();
	String getTapaRanura();
	Integer getPotenciaContratada();
	Long getSectorTipico();
}
