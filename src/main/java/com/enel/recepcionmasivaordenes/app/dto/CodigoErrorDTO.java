package com.enel.recepcionmasivaordenes.app.dto;

public interface CodigoErrorDTO {

	String getValor();
	String getDescripcion();
	
}
