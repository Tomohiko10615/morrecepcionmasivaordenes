package com.enel.recepcionmasivaordenes.app;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class RecepcionMasivaOrdenesApplication implements CommandLineRunner {

	@Autowired
	RecepcionMasivaOrdenes app;
	
	@Autowired
	Map<String,DataSource> datasources;
	
	public static void main(String[] args) {
		SpringApplication.run(RecepcionMasivaOrdenesApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("Validado parámetros de entrada...");
		
		if ( args.length < 4 || args.length > 5 ) {
			log.error("\nUse:\n"
					+ "<> \n"
					+ "<Cod. Empresa> \n"
					+ "<path sin '/'> \n"
					+ "<archivo de entrada> \n"
					+ "<usuario receptor> \n"
					+ "<path archivo donde mover (opcional)>");
			System.exit(1);
		}
		
		log.info("Obteniendo parámetros...");
		
		app.getStParametros().setACodPartition(args[0]);
		app.getStParametros().setACodUsuario(args[3]);
		app.getStParametros().setAArchivoEnt(args[1] + "/" + args[2]);
		
		boolean existeDirSal = args.length == 5;
		
		if (existeDirSal) {
			app.getStParametros().setADirSal(args[4]);
		}
		
		log.info("Se obtuvo los parámetros: {}", app.getStParametros());
		
		//app.obtenerValoresEstados();
		
		if (!app.validarParametros(existeDirSal)) {
			log.error("Error al validar los parámetros");
			System.exit(1);
		}
		
		log.info("Verificando si el proceso se encuentra en ejecución...");
		
		if (!app.bloquearEscritura()) {
			System.exit(1);
		}
		
		log.info("Abriendo conexión a la base de datos...");
		
		DataSource synergiaDS = datasources.get("synergiaDataSource");
		DataSource scomDS = datasources.get("scomDataSource");
		
		app.setCnxSynergia(synergiaDS.getConnection());
		app.getCnxSynergia().setAutoCommit(false);
		
		app.setJdbcTemplateOracle(new JdbcTemplate(new SingleConnectionDataSource(app.getCnxSynergia(), true)));
		
		app.setCnxScom(scomDS.getConnection());
		app.getCnxScom().setAutoCommit(false);
		
		app.setJdbcTemplatePostgres(new JdbcTemplate(new SingleConnectionDataSource(app.getCnxScom(), true)));
		
		log.info("Iniciando el proceso...");
		
		if(!app.ejecutarProceso()) {
	        app.getCnxSynergia().rollback();
	        app.getCnxSynergia().rollback();
	        app.setIRet(1);
	    } else {
	        app.getCnxSynergia().commit();
	        app.getCnxScom().commit();
	    }

		if (app.getIRet() == 0) {
			log.info("Programa finalizado con éxito");
		}
		
		
		
		System.exit(app.getIRet());
	}

}
