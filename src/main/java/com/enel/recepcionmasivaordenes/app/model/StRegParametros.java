package com.enel.recepcionmasivaordenes.app.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StRegParametros {

	private String aCodPartition;
    private String aArchivoEnt;
    private String aArchivo;
    private String aArchivoSinExt;
    private String aDirEnt;
    private String aDirSal;
    private String cFecDesde;
    private String cFecHasta;
    private String aCodUsuario;
    private Long iIdUsuario;
    private Integer iEmpresa;
}
