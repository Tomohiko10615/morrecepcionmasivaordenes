package com.enel.recepcionmasivaordenes.app.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StRegDatos {

	private String aCodMotivo;
	private String aCodSubtipoOrden;
	private Long dIdMotivo;
	private Long dIdSubtipoOrden;
	private Long dIdWorkflow;
	private String aVigencia;
	private String aIdState;
	private String aCodInterno;
	private String numeroOrden;
	private String aCodTipoOrden;
	private String codEjecutor;
	private Long dNroServicio;
	private String nroComponente;
	private String codMarca;
	private String codModelo;
	private String codSitTerr1;
	private String codSitTerr2;
	private String codAccRealizada;
	private String fechaEjec;
	private String observacion;
	private String lecturas;
	private Long dIdTipoOrden;
	private Long dIdOrden;
	private Long dIdWorkflowOrd;
	private Long dsecMagnitud;
	private Long dIdServicio;
	private Long dIdWorkflowSE;
	private Long dIdEjecutor;
	private Long dIdComponente;
	private Long dIdMedida;
	private Double dEnteros;
	private Double dDecimales;
	private Double dFactor;
	private Long dIdTipoCalculo;
	private Long dIdSitTerr1;
	private String sCodInternoSitTerr1;
	private Long dIdSitTerr2;
	private String sCodInternoSitTerr2;
	private Long dIdAccRealizada;
	private String sCodInternoAccion;
	private String sNewEstadoSE;
	private Long idCargo;
	private Long idCodValor;
	private String aCodGart;
}
